const FavNumber = artifacts.require("./FavNumber");

contract("FavNumber", (accounts) => {
  it("Should change the fav number in the blockchain", async function () {
    const Contract = await FavNumber.deployed();
    const res = await Contract.setFavNumber(47, { from: accounts[0] });
  });

  it("Should get the fav number in the blockchain", async function () {
    const Contract = await FavNumber.deployed();
    const number = await Contract.getFavNumber();
    assert.equal(number.words[0], 47, "Not equal to 47");
  });
});
