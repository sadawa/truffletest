// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

contract FavNumber {
  uint favNumber;

  function getFavNumber() external view returns(uint){
    return favNumber;
  }

  function setFavNumber(uint _favNumber) external {
    favNumber = _favNumber;
  }
}
